﻿namespace KingsStoreApi.Services.Interfaces
{
    public interface ITransactionService
    {
        void PayForProduct();
    }
}
