﻿using KingsStoreApi.Helpers.Implementations;
using KingsStoreApi.Model.DataTransferObjects.CategoryServicesDTO;
using KingsStoreApi.Services.Interfaces;

namespace KingsStoreApi.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        public ReturnModel CreateCategory(CreateCategoryDTO model)
        {
            throw new System.NotImplementedException();
        }

        public ReturnModel GetAllCategories()
        {
            throw new System.NotImplementedException();
        }

        public ReturnModel GetCategory(string categoryName)
        {
            throw new System.NotImplementedException();
        }

        public ReturnModel SoftDeleteCategory(string id)
        {
            throw new System.NotImplementedException();
        }

        public ReturnModel UpdateCategory(UpdateCategoryDTO model)
        {
            throw new System.NotImplementedException();
        }
    }
}
