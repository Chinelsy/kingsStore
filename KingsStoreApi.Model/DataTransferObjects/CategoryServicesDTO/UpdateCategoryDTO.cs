﻿namespace KingsStoreApi.Model.DataTransferObjects.CategoryServicesDTO
{
    public class UpdateCategoryDTO
    {
        public string Name { get; set; }
        public string Summary { get; set; }
    }
}
