﻿namespace KingsStoreApi.Model.DataTransferObjects.CategoryServicesDTO
{
    public class CreateCategoryDTO
    {
        public string Name { get; set; }
        public string Summary { get; set; }
    }
}
