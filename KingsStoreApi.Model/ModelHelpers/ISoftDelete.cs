﻿namespace KingsStoreApi.Model.ModelHelpers
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }

    }
}
